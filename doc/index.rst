ewoksixs |version|
==================

*ewoksixs* provides workflows for simulation and analysis of Inelastic X-ray Scattering.

*ewoksixs* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.eu/>`_.

How to install
--------------

Install the `ewoksixs <https://ewoksixs.readthedocs.io/en/latest/>`_ Python package

.. code:: bash

    pip install ewoksixs

We can start the ewoks canvas with

Linux
******

.. code:: bash

    ewoks-canvas

Windows
*******

.. code:: bash

    ewoks-canvas.exe

Documentation
-------------

.. toctree::
    :maxdepth: 2

    api
