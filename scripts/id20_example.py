import os
import shutil
from collections import OrderedDict
import matplotlib.pyplot as plt

from ixstools.scan import Scan, SpecPath
from ixstools.simple_roi_finder_gui import ZoomRoiFinderGui
from ixstools.roi import Roi
from ixstools.axis import AxisXRSsum
from ixstools.collector import BaseCollector
from ixstools.spectrum import XRSSpectrum


def run(interactive=False):
    scan_path = SpecPath(
        path_to_spec="/data/id21/inhouse/wout/ixstools/test_data",
        spec_file_name="hydra",
        path_to_edf="/data/id21/inhouse/wout/ixstools/test_data/edf",
        images_prefix="hydra",
        images_postfix=".edf",
    )

    outdir = os.path.join(os.path.realpath(os.path.dirname(__file__)), "results")
    os.makedirs(outdir, exist_ok=True)
    org_roi_filename = os.path.join(scan_path.path_to_spec, "rois", "rois_scan_102.h5")
    roi_filename = os.path.join(outdir, "rois_scan_102.h5")
    shutil.copyfile(org_roi_filename, roi_filename)
    out_filename = os.path.join(outdir, "test_for_ewoks.dat")

    # make the path default for all following scans
    Scan.set_default_scan_path(scan_path)

    #############################################################
    # sample containing graphite
    #############################################################

    #########################
    # region of interest
    #########################
    if not os.path.exists(roi_filename):
        above_list = [102, 106]
        scan_above = Scan()
        scan_above.load(above_list[0])
        for ii in above_list[1::]:
            _tmp = Scan()
            _tmp.load(ii)
            scan_above._det_images += _tmp._det_images
        im_a = scan_above._det_images.sum(axis=0)
        zroi = ZoomRoiFinderGui(input_image=im_a, log_scaling=True)
        # roi_gui = SimpleRoiFinderGui()
        # roi_gui.gui_session( im_a )
        roi = zroi.roi_obj
        roi.save(roi_filename)
    else:
        roi = Roi()
        roi.load(roi_filename)

    ###################################
    # definition of energy loss axis
    ###################################
    scan_e = Scan()
    scan_e.load(99, roi=roi)
    axis_xrs = AxisXRSsum()
    axis_xrs.configure(scan_e)

    ############################################
    # integration of data at the Carbon K-edge
    ############################################
    scan_num_list = [102, 106]

    scan_list = []
    for ii in scan_num_list:
        _tmp = Scan()
        _tmp.load(ii, roi=roi)
        scan_list.append(_tmp)

    axis_list = [axis_xrs]
    axis_choices = [{"placement_mode": "grid"}]
    collector = BaseCollector(
        axis_list,
        abscissa_choices=axis_choices,
        do_interpolation=[True],
        # normalisation_renorm=1e-6,
    )
    data_1, error_1, coordinates_1 = collector.get_result_from_scan_sequence(scan_list)

    if interactive:
        plt.ion()
        for ii in range(len(data_1.keys())):
            plt.errorbar(
                coordinates_1[f"sum_abscissa_{ii}"],
                data_1[f"sum_{ii}"],
                error_1[f"sum_error_{ii}"],
            )

        plt.show()
    else:
        import h5py

        os.makedirs(os.path.join(os.path.dirname(__file__), "results"), exist_ok=True)
        with h5py.File(
            os.path.join(os.path.dirname(__file__), "results", "output.h5"), "w"
        ) as h5file:
            for k, v in data_1.items():
                h5file.create_dataset(k, data=v)

    # FIRST IMPLEMENTATION CAN STOP THERE
    return

    # #############################################################
    # # background subtraction (still somewhat of a spaghetti code)
    # #############################################################

    lowq_list = list(range(24))
    lowq_list.extend(list(range(36, 60)))
    highq_list = list(range(24, 36))
    highq_list.extend(list(range(60, 72)))

    roikey_groups = OrderedDict([["lowq", lowq_list], ["highq", highq_list]])
    # NOT USED? background_model = "atomicHF"
    chem_formula = "C"

    sum_spec = XRSSpectrum(
        data_1,
        error_1,
        coordinates_1,
        example_scan=_tmp,
        xrs_axis=axis_xrs,
        chem_formula=chem_formula,
    )
    sum_spec._calculate_averages(roikey_groups, error_weighing=False)

    # Carbon K-edge
    sum_spec.remove_background(
        "lowq",
        "C",
        "K",
        [[268.0, 283.5], [315.0, 340.0]],
        model="pearson7",
        core_shift=6,
        guess=[37.568, 124.536, 0.097, 0.007, 0.000, -0.003, 2.2],
        weights=[2, 1],
        regularization=0.0,
        verbose=False,
    )

    sum_spec.remove_background(
        "highq",
        "C",
        "K",
        [[268.0, 283.5], [315.0, 340.0]],
        model="pearson7",
        core_shift=6.0,
        guess=[300, 200, 1.0000e00, 5.0000e-03, 0.0, 0.0, 5.000],
        weights=[2, 1],
        regularization=0.0,
        verbose=False,
    )

    #####################################
    # save background subtracted data
    #####################################
    if 1:
        sum_spec.export_Sqw(
            out_filename,
            roi_keys=["lowq", "highq"],
            file_format="ascii",
            header="# ",
            comments="# ",
        )


if __name__ == "__main__":
    run()
