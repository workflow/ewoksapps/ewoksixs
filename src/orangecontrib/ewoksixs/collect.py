import json
import logging

from ewoksorange.bindings import OWEwoksWidgetOneThread
from ewoksorange.bindings import ow_build_opts
from ewoksorange.gui.parameterform import ParameterForm

from ewokscore.missing_data import is_missing_data
from silx.gui.plot import PlotWindow

from ewoksixs.tasks.collect_resuts_along_axes import CollectResultsAlongAxes

_logger = logging.getLogger(__name__)


__all__ = ["OWCollect"]


class OWCollect(
    OWEwoksWidgetOneThread, **ow_build_opts, ewokstaskclass=CollectResultsAlongAxes
):
    name = "Collect results along axes"
    description = "Collect results along axes"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._init_control_area()
        self._init_main_area()

    def _init_main_area(self):
        super()._init_main_area()
        layout = self._get_main_layout()

        self._plot_widget = PlotWindow(parent=self.mainArea)
        self._plot_widget
        layout.addWidget(self._plot_widget)

    def _init_control_area(self) -> None:
        super()._init_control_area()
        values = self.get_default_input_values(include_missing=True)
        self.update_default_inputs(**values)

        self._default_inputs_form = ParameterForm(parent=self.controlArea)

        parameters = {
            "roi_filename": {
                "label": "HDF5 file containing the ROI",
                "value_for_type": "",
                "select": "file",
            },
            "scan_numbers": {
                "label": "List of scan numbers to process",
                "value_for_type": "",
                "serialize": json.dumps,
                "deserialize": json.loads,
            },
        }

        for name, kw in parameters.items():
            self._default_inputs_form.addParameter(
                name,
                value=values[name],
                value_change_callback=self._default_inputs_changed,
                **kw,
            )

    def _update_input_widgets(self):
        values = self.get_task_input_values()
        self._default_inputs_form.set_parameter_values(values)
        dynamic = self.get_dynamic_input_names()
        for name in self.get_input_names():
            self._default_inputs_form.set_parameter_enabled(name, name not in dynamic)

    def _default_inputs_changed(self):
        form_values = self._default_inputs_form.get_parameter_values()

        self.update_default_inputs(**form_values)
        self._update_input_widgets()

    def handleNewSignals(self) -> None:
        self._update_input_widgets()
        super().handleNewSignals()

    def task_output_changed(self):
        coordinates = self.get_task_output_value("coordinates")
        data = self.get_task_output_value("data")
        errors = self.get_task_output_value("errors")

        if (
            is_missing_data(coordinates)
            or is_missing_data(data)
            or is_missing_data(errors)
        ):
            _logger.warning("Missing outputs ! Perhaps the task execution failed ?")
            return

        self._plot_widget.clearCurves()

        for i_c, i_d, i_e in zip(coordinates.keys(), data.keys(), errors.keys()):
            if data[i_d].ndim > 1:
                _logger.warning(
                    "Cannot display nD data with n > 1 for now. Skipping the plot"
                )
                return

            self._plot_widget.addCurve(
                x=coordinates[i_c],
                y=data[i_d],
                yerror=errors[i_e],
                legend=str(i_d),
            )
