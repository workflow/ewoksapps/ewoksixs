from __future__ import annotations
import os.path

from ewoksorange.gui.parameterform import ParameterForm


from ewoksorange.bindings import OWEwoksWidgetNoThread
from silx.gui.qt import QTreeView, QFileSystemModel

from ewoksixs.tasks.define_scan_path import DefineScanPath


__all__ = ["OWDefineScanPath"]


class OWDefineScanPath(OWEwoksWidgetNoThread, ewokstaskclass=DefineScanPath):
    name = "Define scan path"
    description = "Define the scan path object. Only SPEC for now."

    def __init__(self):
        super().__init__()
        self._init_main_area()
        self._init_control_area()

    def _init_control_area(self) -> None:
        super()._init_control_area()
        self._parameter_form = ParameterForm(parent=self.controlArea)
        values = self.get_default_input_values(include_missing=True)
        self.update_default_inputs(**values)

        self._parameter_form.addParameter(
            "path_to_spec",
            label="SPEC folder",
            value_change_callback=self._parameters_changed,
            value_for_type="",
            select="directory",
        )

        self._parameter_form.addParameter(
            "spec_file_name",
            label="SPEC file name",
            value_change_callback=self._parameters_changed,
            value_for_type="",
        )
        self._update_input_widgets()
        self._set_main_browser_directory()

    def _init_main_area(self):
        super()._init_main_area()
        layout = self._get_main_layout()

        self._file_view = QTreeView(parent=self.mainArea)
        self._file_model = QFileSystemModel()
        self._file_view.setModel(self._file_model)
        layout.addWidget(self._file_view)

    def _parameters_changed(self):
        form_values = self._parameter_form.get_parameter_values()

        self.update_default_inputs(
            spec_path_params={
                "path_to_spec": form_values["path_to_spec"],
                "spec_file_name": form_values["spec_file_name"],
            }
        )
        self._set_main_browser_directory()

    def _update_input_widgets(self):
        default_inputs = self.get_default_input_values()
        spec_path_params: dict | None = default_inputs.get("spec_path_params")

        if not spec_path_params:
            return

        path_to_spec = spec_path_params.get("path_to_spec")
        if path_to_spec:
            self._parameter_form.set_parameter_value("path_to_spec", path_to_spec)

        spec_file_name = spec_path_params.get("spec_file_name")
        if spec_file_name:
            self._parameter_form.set_parameter_value("spec_file_name", spec_file_name)

    def _set_main_browser_directory(self):
        dir_name = self.get_task_input_value("spec_path_params", dict()).get(
            "path_to_spec"
        )

        if not dir_name or not os.path.isdir(dir_name):
            return
        self._file_model.setRootPath(dir_name)
        self._file_view.setRootIndex(self._file_model.index(dir_name))

    def handleNewSignals(self) -> None:
        self._update_input_widgets()
        super().handleNewSignals()
