from __future__ import annotations
from silx.gui import qt
from ewoksorange.bindings import OWEwoksWidgetNoThread
from ewoksorange.gui.parameterform import ParameterForm


from ewoksixs.gui.axis_widget import AxisWidget
from ewoksixs.tasks.configure_axes import ConfigureAxes


__all__ = ["OWAxesConfigurator"]


class OWAxesConfigurator(OWEwoksWidgetNoThread, ewokstaskclass=ConfigureAxes):
    name = "Axes configurator"
    description = "Configure axes for collection"
    want_main_area = False

    def __init__(self):
        super().__init__()
        self._axis_widgets: list[AxisWidget] = []
        self._init_control_area()

    def _init_control_area(self) -> None:
        super()._init_control_area()

        self._default_inputs_form = ParameterForm(parent=self.controlArea)
        self._default_inputs_form.addParameter(
            "elastic_scan_number",
            label="Elastic scan number",
            value_for_type=0,
            value_change_callback=self._inputs_changed,
        )
        self._default_inputs_form.addParameter(
            "roi_filename",
            label="HDF5 file containing the ROI",
            value_for_type="",
            select="file",
            value_change_callback=self._inputs_changed,
        )

        self._axis_layout = qt.QVBoxLayout()
        self._get_control_layout().addLayout(self._axis_layout)

        button_layout = qt.QHBoxLayout()
        add_button = qt.QPushButton("Add a new axis")
        add_button.clicked.connect(self._add_axis_widget)
        remove_button = qt.QPushButton("Remove the last axis")
        remove_button.clicked.connect(self._remove_axis_widget)
        button_layout.addWidget(add_button)
        button_layout.addWidget(remove_button)
        self._get_control_layout().addLayout(button_layout)

        self._set_initial_values()

    def _set_initial_values(self):
        values = self.get_default_input_values(include_missing=False)
        self._default_inputs_form.set_parameter_values(values)

        axis_parameters = values.get("axis_parameters", [])
        axis_choices = values.get("axis_choices", [])

        for parameters, axis_choice in zip(axis_parameters, axis_choices):
            self._add_axis_widget()
            axis_widget = self._axis_widgets[-1]
            axis_widget.setName(parameters["name"])
            axis_widget.setParameters(parameters)
            axis_widget.setAbscissaChoice(axis_choice)

    def _add_axis_widget(self):
        new_axis_widget = AxisWidget(title=f"Axis {len(self._axis_widgets)}")
        self._axis_widgets.append(new_axis_widget)
        self._axis_layout.addWidget(new_axis_widget)
        new_axis_widget.valuesChanged.connect(self._inputs_changed)
        self._inputs_changed()

    def _remove_axis_widget(self):
        if len(self._axis_widgets) == 0:
            return
        axis_widget = self._axis_widgets.pop()
        self._axis_layout.removeWidget(axis_widget)

    def _inputs_changed(self):
        elastic_scan_number = self._default_inputs_form.get_parameter_value(
            "elastic_scan_number"
        )
        roi_filename = self._default_inputs_form.get_parameter_value("roi_filename")

        axis_parameters = []
        axis_choices = []
        for axis_widget in self._axis_widgets:
            axis_parameters.append(
                {"name": axis_widget.getName(), **axis_widget.getParameters()}
            )
            axis_choices.append(axis_widget.getAbscissaChoice())

        self.update_default_inputs(
            elastic_scan_number=elastic_scan_number,
            roi_filename=roi_filename,
            axis_parameters=axis_parameters,
            axis_choices=axis_choices,
        )

    def _handle_dynamic_inputs(self):
        values = self.get_task_input_values()
        self._default_inputs_form.set_parameter_values(values)
        dynamic = self.get_dynamic_input_names()
        for name in self._default_inputs_form.get_parameter_names():
            self._default_inputs_form.set_parameter_enabled(name, name not in dynamic)

    def handleNewSignals(self) -> None:
        self._inputs_changed()
        super().handleNewSignals()
