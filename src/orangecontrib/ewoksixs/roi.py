from typing import Optional
import h5py
import json
import logging
from ewokscore import missing_data
from ixstools.scan import SpecPath
from ewoksorange.bindings import OWEwoksWidgetNoThread
from ewoksorange.bindings.invalid_data import is_invalid_data
from ewoksorange.gui.orange_imports import Input
from ewoksorange.gui.parameterform import ParameterForm


from ewoksixs.gui.roi_picker import RoiPicker
from ewoksixs.tasks.save_roi import GenerateRoiFilename
from ewoksixs.utils import sum_scans

_logger = logging.getLogger(__name__)

__all__ = ["OWRoiPicker"]


class OWRoiPicker(OWEwoksWidgetNoThread, ewokstaskclass=GenerateRoiFilename):
    name = "ROI picker"
    description = "Draw ROIs and save them in a file"

    class Inputs:
        scan_numbers = Input("scan_numbers", object)
        scan_path = Input("scan_path", object)

    def __init__(self):
        super().__init__()
        self._init_control_area()
        self._init_main_area()
        self._scan_path: Optional[SpecPath] = None

    def _init_control_area(self) -> None:

        self._parameter_form = ParameterForm(parent=self.controlArea)
        values = self.get_default_input_values(include_missing=True)
        self.update_default_inputs(**values)

        self._parameter_form.addParameter(
            "output_filename",
            label="Output filename",
            value=values["output_filename"],
            value_change_callback=self._output_filename_changed,
            value_for_type="",
            select="file",
        )

        self._parameter_form.addParameter(
            "scan_numbers",
            label="Scan numbers",
            value_change_callback=self._inputs_changed,
            value_for_type="",
            serialize=json.dumps,
            deserialize=json.loads,
        )

    def _init_main_area(self):
        super()._init_main_area()
        layout = self._get_main_layout()

        self._picker = RoiPicker(parent=self.mainArea)
        self._picker.setVisible(True)
        self._picker.pickingFinished.connect(self.onPickingFinished)
        layout.addWidget(self._picker)
        layout.setStretchFactor(self._picker, 1)

    def _output_filename_changed(self):
        self.update_default_inputs(
            output_filename=self._parameter_form.get_parameter_value("output_filename")
        )

    @Inputs.scan_numbers
    def set_scan_numbers(self, value):
        value = self._extract_value(value)
        if is_invalid_data(value):
            self._parameter_form.set_parameter_enabled("scan_numbers", True)
        else:
            self._parameter_form.set_parameter_value("scan_numbers", value)
            self._parameter_form.set_parameter_enabled("scan_numbers", False)
        self._inputs_changed()

    @Inputs.scan_path
    def set_scan_path(self, value):
        value = self._extract_value(value)
        if is_invalid_data(value):
            self._scan_path = None
        else:
            self._scan_path = value
        self._inputs_changed()

    def _inputs_changed(self):
        scan_numbers = self._parameter_form.get_parameter_value("scan_numbers")
        scan_path = self.get_task_input_value("scan_path")

        if missing_data.is_missing_data(scan_numbers) or missing_data.is_missing_data(
            scan_path
        ):
            return
        try:
            img = sum_scans(scan_numbers, scan_path)
        except ValueError:
            return
        else:
            self._picker.setImage(img)

    def onPickingFinished(self):
        self.update_default_inputs(**{"roi_list": self._picker.getRois()})
        output_filename = self.get_default_input_values(include_missing=True)[
            "output_filename"
        ]

        if missing_data.is_missing_data(output_filename):
            _logger.error("Please supply an output filename")
            return

        self.execute_ewoks_task()
        self.saveImage(output_filename)

    def saveImage(self, output_filename):
        with h5py.File(output_filename, mode="a") as h5file:
            h5file["input_image"] = self._picker.getImage()

    def handleNewSignals(self) -> None:
        self._inputs_changed()
        super().handleNewSignals()
