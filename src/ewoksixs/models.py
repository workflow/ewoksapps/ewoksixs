from __future__ import annotations
from dataclasses import dataclass
from typing import Union
from ixstools.axis import AxisXRSsum, AxisXES, MetaDataAxis

IxsAxis = Union[AxisXRSsum, AxisXES, MetaDataAxis]

AXES = {
    "sum": AxisXRSsum,
    "vonHamos": AxisXES,
    "metadata": MetaDataAxis,
}


PLACEMENT_MODES = ("grid",)

METADATA_AXIS_OPTIONS = ("energy", "stx", "sty", "stz")


@dataclass
class AxisInput:
    value: IxsAxis
    abscissa_choice: dict[str, str] | None
