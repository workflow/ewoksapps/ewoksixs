from __future__ import annotations
import os.path
import pytest

from ewoksorange.tests.utils import execute_task
from ixstools.scan import SpecPath

from ewoksixs.models import AXES, AxisInput
from ewoksixs.tasks.configure_axes import ConfigureAxes
from orangecontrib.ewoksixs.axes import OWAxesConfigurator

from .utils import data_dir


@pytest.mark.skipif(
    not os.path.exists(data_dir), reason=f"{data_dir} cannot be reached"
)
@pytest.mark.parametrize("task_cls", [OWAxesConfigurator, ConfigureAxes])
def test_axes(task_cls):

    axis_parameters = [
        {"name": "sum"},
        {"name": "metadata", "formula": "energy"},
    ]
    axis_choices = [{"placement_mode": "grid"}, None]
    roi_filename = os.path.join(data_dir, "rois", "rois_scan_102.h5")

    inputs = {
        "axis_parameters": axis_parameters,
        "axis_choices": axis_choices,
        "elastic_scan_number": 99,
        "scan_path": SpecPath(path_to_spec=data_dir, spec_file_name="hydra"),
        "roi_filename": roi_filename,
    }

    outputs = execute_task(task_cls, inputs)
    axes: list[AxisInput] = outputs["axes"]

    assert len(axes) == len(axis_parameters)
    assert len(axes) == len(axis_choices)
    for ax, parameters, abscissa_choice in zip(axes, axis_parameters, axis_choices):
        name = parameters["name"]
        assert isinstance(ax.value, AXES[name])
        assert ax.abscissa_choice == abscissa_choice
