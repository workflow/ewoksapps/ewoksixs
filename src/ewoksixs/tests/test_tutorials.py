from ewoksorange.bindings import ows_to_ewoks
from ewokscore import execute_graph

import h5py
import numpy
import os
import pytest

try:
    from importlib import resources
except ImportError:
    import importlib_resources as resources

from .utils import data_dir


@pytest.mark.skipif(
    not os.path.exists(data_dir), reason=f"{data_dir} cannot be reached"
)
def test_sum_axis_collection():
    from orangecontrib.ewoksixs import tutorials

    inputs = [
        {
            "name": "spec_path_params",
            "value": {
                "path_to_spec": "/data/id21/inhouse/wout/ixstools/test_data",
                "spec_file_name": "hydra",
            },
            "all": True,
        },
        {
            "name": "scan_numbers",
            "value": [102, 106],
            "all": True,
        },
        {
            "name": "roi_filename",
            "value": "/data/id21/inhouse/wout/ixstools/test_data/rois/rois_scan_102.h5",
            "all": True,
        },
        {
            "name": "elastic_scan_number",
            "value": 99,
            "all": True,
        },
        {
            "name": "axis_choices",
            "value": [None],
            "all": True,
        },
        {
            "name": "axis_parameters",
            "value": [{"name": "sum"}],
            "all": True,
        },
    ]

    with resources.path(tutorials, "sum_axis_collection.ows") as filename:
        graph = ows_to_ewoks(filename)
        outputs = execute_graph(graph, inputs=inputs, outputs=[{"all": True}])

        data = outputs["data"]

        with h5py.File(
            os.path.join(os.path.dirname(__file__), "data", "collect_output.h5"), "r"
        ) as expected_data:
            assert sorted(expected_data.keys()) == sorted(list(data.keys()))
            for k in data.keys():
                numpy.testing.assert_allclose(data[k], expected_data[k][()])
