import os.path
import numpy
import h5py

from silx.gui.plot.items.roi import RectangleROI


from ewoksorange.tests.utils import execute_task

from ewoksixs.tasks.save_roi import GenerateRoiFilename


def test_roi(tmpdir):
    roi = RectangleROI()
    roi.setOrigin((0, 0))
    roi.setSize((3, 4))

    roi2 = RectangleROI()
    roi2.setOrigin((1, 0))
    roi2.setSize((2, 5))

    inputs = {
        "roi_list": [roi, roi2],
        "output_filename": os.path.join(tmpdir, "roi_test.h5"),
    }

    outputs = execute_task(GenerateRoiFilename, inputs)

    assert inputs["output_filename"] == outputs["roi_filename"]
    with h5py.File(outputs["roi_filename"], "r") as h5file:
        numpy.testing.assert_allclose(h5file["col"], [0, 1, 2, 1, 2])
        numpy.testing.assert_allclose(h5file["row"], [0, 1, 2, 3, 0, 1, 2, 3, 4])
