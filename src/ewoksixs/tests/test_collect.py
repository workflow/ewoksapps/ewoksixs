import h5py
import os.path
import pytest

from ewoksorange.tests.utils import execute_task
from ixstools.scan import SpecPath
from ixstools.axis import MetaDataAxis

from ewoksixs.models import AxisInput
from ewoksixs.tasks.collect_resuts_along_axes import CollectResultsAlongAxes
from orangecontrib.ewoksixs.collect import OWCollect

from .utils import data_dir, get_sum_axis


@pytest.mark.skipif(
    not os.path.exists(data_dir), reason=f"{data_dir} cannot be reached"
)
@pytest.mark.parametrize("task_cls", [OWCollect, CollectResultsAlongAxes])
def test_collect(task_cls):

    roi_filename = os.path.join(data_dir, "rois", "rois_scan_102.h5")
    with h5py.File(roi_filename, "r") as h5file:
        n_rois = h5file["rois_start_end"].shape[0]

    scan_path = SpecPath(path_to_spec=data_dir, spec_file_name="hydra")

    inputs = {
        "roi_filename": roi_filename,
        "axes": [
            AxisInput(
                value=get_sum_axis(scan_path, roi_filename),
                abscissa_choice={"placement_mode": "grid"},
            )
        ],
        "scan_numbers": [102, 106],
        "scan_path": SpecPath(path_to_spec=data_dir, spec_file_name="hydra"),
    }

    outputs = execute_task(task_cls, inputs)
    out_coordinates = outputs["coordinates"]
    out_data = outputs["data"]
    out_errors = outputs["errors"]

    assert out_coordinates.shape[-1] == n_rois
    assert out_data.shape[-1] == n_rois
    assert out_errors.shape[-1] == n_rois


@pytest.mark.skipif(
    not os.path.exists(data_dir), reason=f"{data_dir} cannot be reached"
)
@pytest.mark.parametrize("task_cls", [OWCollect, CollectResultsAlongAxes])
def test_collect_multiple_axes(task_cls):

    roi_filename = os.path.join(data_dir, "rois", "rois_scan_102.h5")
    with h5py.File(roi_filename, "r") as h5file:
        n_rois = h5file["rois_start_end"].shape[0]

    scan_path = SpecPath(path_to_spec=data_dir, spec_file_name="hydra")

    inputs = {
        "roi_filename": roi_filename,
        "axes": [
            AxisInput(
                value=get_sum_axis(scan_path, roi_filename),
                abscissa_choice={"placement_mode": "grid"},
            ),
            AxisInput(value=MetaDataAxis(formula="energy"), abscissa_choice=None),
        ],
        "scan_numbers": [102, 106],
        "scan_path": SpecPath(path_to_spec=data_dir, spec_file_name="hydra"),
    }

    outputs = execute_task(task_cls, inputs)
    out_coordinates = outputs["coordinates"]
    out_data = outputs["data"]
    out_errors = outputs["errors"]

    assert len(out_coordinates) == n_rois
    assert len(out_data) == n_rois
    assert len(out_errors) == n_rois
