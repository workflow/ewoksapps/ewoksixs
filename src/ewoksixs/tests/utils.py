from __future__ import annotations
from ewoksixs.utils import load_roi, load_scan
from ixstools.scan import SpecPath
from ixstools.axis import AxisXRSsum


data_dir = "/data/id21/inhouse/wout/ixstools/test_data"


def get_sum_axis(scan_path: SpecPath, roi_filename: str):
    roi = load_roi(roi_filename)

    axis = AxisXRSsum()
    elastic_scan = load_scan(99, scan_path, roi)
    axis.configure(elastic_scan)

    return axis
