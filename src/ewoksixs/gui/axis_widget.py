from __future__ import annotations
from typing import Any
from silx.gui import qt
from ewoksorange.gui.parameterform import ParameterForm
from ewokscore.missing_data import is_missing_data


from ..models import AXES, METADATA_AXIS_OPTIONS, PLACEMENT_MODES


class AxisWidget(qt.QGroupBox):
    """A widget to configure a single axis (type and abscissa choice)"""

    valuesChanged = qt.Signal()

    def __init__(self, title=None, parent=None) -> None:
        super().__init__(parent)
        self.setFlat(True)
        self.setTitle(title)

        layout = qt.QVBoxLayout()

        axis_type_layout = qt.QHBoxLayout()
        axis_type_label = qt.QLabel("Type")
        self._axis_type_box = qt.QComboBox()
        self._axis_type_box.addItems(list(AXES.keys()))
        self._axis_type_box.currentTextChanged.connect(self.updateAxisParameterForm)
        self._axis_type_box.currentTextChanged.connect(self.valuesChanged.emit)
        axis_type_layout.addWidget(axis_type_label)
        axis_type_layout.addWidget(self._axis_type_box)
        layout.addLayout(axis_type_layout)

        abscissa_choice_layout = qt.QHBoxLayout()
        abscissa_choice_label = qt.QLabel("Abscissa choice")
        self._abscissa_choice_box = qt.QComboBox()
        self._abscissa_choice_box.addItems(["None", *PLACEMENT_MODES])
        self._abscissa_choice_box.currentTextChanged.connect(self.valuesChanged.emit)
        abscissa_choice_layout.addWidget(abscissa_choice_label)
        abscissa_choice_layout.addWidget(self._abscissa_choice_box)
        layout.addLayout(abscissa_choice_layout)

        self.setLayout(layout)
        self._parameter_form = None

    def getName(self) -> str:
        return self._axis_type_box.currentText()

    def setName(self, text: str):
        return self._axis_type_box.setCurrentText(text)

    def getAbscissaChoice(self) -> dict[str, str] | None:
        raw_choice = self._abscissa_choice_box.currentText()

        if raw_choice == "None":
            return None

        return {"placement_mode": raw_choice}

    def setAbscissaChoice(self, abscissa_choice: dict[str, str] | None):
        if abscissa_choice is None:
            self._abscissa_choice_box.setCurrentText("None")
            return

        placement_mode = abscissa_choice.get("placement_mode")
        if placement_mode in PLACEMENT_MODES:
            self._axis_type_box.setCurrentText(placement_mode)

    def updateAxisParameterForm(self, newValue: str):
        layout = self.layout()
        if layout and self._parameter_form:
            layout.removeWidget(self._parameter_form)

        if newValue == "vonHamos":
            parameter_form = ParameterForm(parent=self)

            parameter_form.addParameter(
                "first", value_for_type=0, value_change_callback=self.valuesChanged.emit
            )
            parameter_form.addParameter(
                "last", value_for_type=0, value_change_callback=self.valuesChanged.emit
            )
            parameter_form.addParameter(
                "fit_rotation",
                value_for_type=False,
                value_change_callback=self.valuesChanged.emit,
            )

        elif newValue == "metadata":
            parameter_form = ParameterForm(parent=self)

            parameter_form.addParameter(
                "formula",
                value_for_type="",
                value_change_callback=self.valuesChanged.emit,
            )
            completer = qt.QCompleter(METADATA_AXIS_OPTIONS)
            parameter_form._get_value_widget("formula").setCompleter(completer)

        else:
            parameter_form = None

        self._parameter_form = parameter_form

    def getParameters(self) -> dict[str, Any]:
        if self._parameter_form is None:
            return {}

        return {
            k: v
            for k, v in self._parameter_form.get_parameter_values().items()
            if not is_missing_data(v)
        }

    def setParameters(self, values: dict[str, Any]):
        if self._parameter_form is None:
            return

        return self._parameter_form.set_parameter_values(values)
