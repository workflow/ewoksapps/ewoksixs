from __future__ import annotations
from silx.gui import qt
from silx.gui.colors import Colormap
from silx.gui.plot.tools.roi import RegionOfInterestManager
from silx.gui.plot.tools.roi import RegionOfInterestTableWidget
from silx.gui.plot.items.roi import RectangleROI
from silx.gui.plot import PlotWindow


class RoiPicker(qt.QWidget):
    pickingFinished = qt.Signal()

    def __init__(self, img=None, parent=None) -> None:
        super().__init__(parent)

        self._img_plot = PlotWindow(self, roi=False)
        self._img_plot.setDefaultColormap(Colormap("Blues", normalization="log"))
        if img is not None:
            self._img_plot.addImage(img)

        self._roiManager = RegionOfInterestManager(self._img_plot)
        self._roiManager.setColor("black")
        roiTable = RegionOfInterestTableWidget()
        roiTable.setRegionOfInterestManager(self._roiManager)

        buttonLayout = qt.QHBoxLayout()
        addButton = qt.QPushButton("Add", self)
        addButton.clicked.connect(self.addRoi)
        exitButton = qt.QPushButton("Finished", self)
        exitButton.clicked.connect(self.pickingFinished.emit)
        buttonLayout.addWidget(addButton)
        buttonLayout.addWidget(exitButton)

        layout = qt.QGridLayout(self)
        layout.setSpacing(0)
        layout.addWidget(self._img_plot, 0, 0)
        layout.addWidget(roiTable, 0, 1, 2, 1)
        layout.addLayout(buttonLayout, 1, 0)

        self.setLayout(layout)

    def addRoi(self):
        for roi in self._roiManager.getRois():
            roi.setEditable(False)
            roi.setSelectable(False)
        roi = RectangleROI()
        x_min, x_max = self._img_plot.getXAxis().getLimits()
        y_min, y_max = self._img_plot.getYAxis().getLimits()
        roi.setGeometry(origin=(x_min, y_min), size=(x_max - x_min, y_max - y_min))
        roi.setEditable(True)
        roi.setSelectable(True)
        self._roiManager.addRoi(roi)
        self._img_plot.resetZoom()

    def setImage(self, img):
        self._img_plot.addImage(img)

    def getImage(self):
        return self._img_plot.getImage().getData(copy=False)

    def getRois(self):
        return self._roiManager.getRois()
