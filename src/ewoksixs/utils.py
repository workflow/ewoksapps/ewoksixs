from __future__ import annotations
from typing import Sequence

from ixstools.scan import Scan, SpecPath
from ixstools.roi import Roi


def load_scan(
    scan_number: int, scan_path: SpecPath | None, roi: Roi | None = None
) -> Scan:
    scan = Scan()
    scan.load(scan_number, scan_path, roi=roi)

    return scan


def load_roi(roi_filename: str) -> Roi:
    roi = Roi()
    roi.load(roi_filename)

    return roi


def sum_scans(scan_numbers: Sequence[int], scan_path: SpecPath):

    summed_image = None
    for scan_num in scan_numbers:
        scan = load_scan(scan_num, scan_path)
        if summed_image is None:
            summed_image = scan._det_images
        else:
            summed_image += scan._det_images

    return summed_image.sum(axis=0)
