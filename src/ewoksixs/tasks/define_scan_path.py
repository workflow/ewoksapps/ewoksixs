from __future__ import annotations

from ewokscore import Task
from ixstools.scan import SpecPath


class DefineScanPath(
    Task,
    input_names=[],
    optional_input_names=["spec_path_params", "bliss_path_params"],
    output_names=["scan_path"],
):

    def run(self):
        spec_path_params: dict | None = self.get_input_value("spec_path_params", None)
        bliss_path_params: dict | None = self.get_input_value("bliss_path_params", None)

        if spec_path_params is None and bliss_path_params is None:
            raise ValueError(
                "At least one type of path (Spec or Bliss) must be provided"
            )

        if spec_path_params is None and bliss_path_params is not None:
            raise ValueError("Bliss paths are not supported yet")

        self.outputs.scan_path = SpecPath(**spec_path_params)
