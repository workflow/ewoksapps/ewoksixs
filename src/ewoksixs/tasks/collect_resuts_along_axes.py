from __future__ import annotations
from typing import Sequence

from ewokscore import Task
from ixstools.collector import BaseCollector
from ixstools.scan import SpecPath


from ..models import AxisInput
from ..utils import load_roi, load_scan


class CollectResultsAlongAxes(
    Task,
    input_names=[
        "roi_filename",
        "axes",
        "scan_numbers",
        "scan_path",
    ],
    output_names=["data", "errors", "coordinates"],
):
    """
    Collect results along supplied axes
    """

    def run(self):
        roi_filename: str = self.inputs.roi_filename
        axes: Sequence[AxisInput] = self.inputs.axes
        scan_numbers: Sequence[int] = self.inputs.scan_numbers
        scan_path: SpecPath = self.inputs.scan_path

        roi = load_roi(roi_filename)

        collector = BaseCollector(
            axis_list=[ax.value for ax in axes],
            abscissa_choices=[ax.abscissa_choice for ax in axes],
            do_interpolation=[True] * len(axes),
        )
        data, errors, coordinates = collector.get_result_from_scan_sequence(
            [load_scan(number, scan_path, roi) for number in scan_numbers]
        )

        self.outputs.data = data
        self.outputs.errors = errors
        self.outputs.coordinates = coordinates
