from __future__ import annotations
from ewokscore import Task
import h5py
import numpy
from silx.gui.plot.items.roi import RectangleROI


class GenerateRoiFilename(
    Task,
    input_names=["roi_list", "output_filename"],
    output_names=["roi_filename"],
):

    def run(self):
        """Adapted from ixstools.roi.Roi.save"""
        roi_list: list[RectangleROI] = self.get_input_value("roi_list")
        output_filename: str = self.get_input_value("output_filename")

        length = 0
        row = []
        col = []
        rois_start_end = []
        for ide, roi in enumerate(roi_list):
            col_origin, row_origin = roi.getOrigin()
            col_size, row_size = roi.getSize()
            rows = [int(row_origin) + row for row in range(int(row_size))]
            cols = [int(col_origin) + col for col in range(int(col_size))]
            row.extend(rows)
            col.extend(cols)
            l = len(rows)  # noqa: E741
            rois_start_end.append([ide, length, length + l])
            length = length + l

        with h5py.File(output_filename, mode="w") as h5file:
            h5file["row"] = numpy.array(row)
            h5file["col"] = numpy.array(col)
            h5file["rois_start_end"] = numpy.array(rois_start_end)

        self.outputs.roi_filename = output_filename
