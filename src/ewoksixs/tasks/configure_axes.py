from __future__ import annotations
from typing import Any, Sequence

from ewokscore import Task
from ixstools.scan import SpecPath

from ..models import AXES, AxisInput
from ..utils import load_roi, load_scan


class ConfigureAxes(
    Task,
    input_names=[
        "axis_parameters",
        "axis_choices",
        "elastic_scan_number",
        "scan_path",
        "roi_filename",
    ],
    output_names=["axes"],
):
    """
    Configure axes for collection
    """

    def run(self):
        axis_parameters: Sequence[dict[str, Any]] = self.inputs.axis_parameters
        axis_choices: Sequence[dict[str, str]] = self.inputs.axis_choices
        elastic_scan_number: int = self.inputs.elastic_scan_number
        scan_path: SpecPath = self.inputs.scan_path
        roi_filename: str = self.inputs.roi_filename

        roi = load_roi(roi_filename)
        elastic_scan = load_scan(elastic_scan_number, scan_path, roi)

        axes = []
        for raw_parameters, abscissa_choice in zip(axis_parameters, axis_choices):
            parameters = {**raw_parameters}
            name = parameters.pop("name")
            if name not in AXES:
                raise KeyError(
                    f"{name} axis is not part of the supported axes ({AXES.keys()})"
                )
            axis = AXES[name]()
            if name == "metadata":
                axis = AXES[name](**parameters)
            else:
                axis = AXES[name]()
                axis.configure(elastic_scan, **parameters)
            axes.append(AxisInput(value=axis, abscissa_choice=abscissa_choice))

        self.outputs.axes = axes
