# ewoksixs

Workflows for simulation and analysis of Inelastic X-ray Scattering

## How to install

Install the [ewoksixs](https://ewoksixs.readthedocs.io/en/latest/) Python package

```
pip install ewoksixs
```
We can start the ewoks canvas with 

### Linux
```
ewoks-canvas 
```

### Windows
```
ewoks-canvas.exe
```

There is a workflow example in _Help/Example Workflows_

## Documentation

https://ewoksixs.readthedocs.io/
